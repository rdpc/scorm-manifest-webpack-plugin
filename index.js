builder = require('xmlbuilder');
function ScormManifestPlugin(options) {
  this.options = options;
}

ScormManifestPlugin.prototype.apply = function(compiler) {
  var self = this;
  compiler.plugin('emit', function (compilation, callback) {
    var xmlObj = {
      manifest: {
        "@identifier": self.options.number,
        "@version": "1",
        "@xmlns": "http://www.imsproject.org/xsd/imscp_rootv1p1p2",
        "@xmlns:adlcp": "http://www.adlnet.org/xsd/adlcp_rootv1p2",
        "@xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "@xsi:schemaLocation": "http://www.imsproject.org/xsd/imscp_rootv1p1p2 imscp_rootv1p1p2.xsd http://www.imsglobal.org/xsd/imsmd_rootv1p2p1 imsmd_rootv1p2p1.xsd http://www.adlnet.org/xsd/adlcp_rootv1p2 adlcp_rootv1p2.xsd",

        metadata: {
          schema: "ADL SCORM",
          schemaversion: "1.2"
        },

        organizations: {
          "@default": self.options.number + "_default_org",
          organization: {
            "@identifier": self.options.number + "_default_org",
            title: self.options.number + " " + self.options.name,
            item: {
              "@identifier": "item_1",
              "@identifierref": "resource_1",
              title: self.options.number
            }
          }
        },

        resources: {
          resource: {
            "@identifier": "resource_1",
            "@type": "webcontent",
            "@adlcp:scormtype": "sco",
            "@href": "index.html",
            file: []
          }
        }
      }
    };

    for (var filename in compilation.assets) {
      xmlObj.manifest.resources.resource.file.push(
        {"@href": filename}
      );
    }
    var xml = builder.create(xmlObj);

    var manifest = xml.end({pretty: true});

    compilation.assets['imsmanifest.xml'] = {
      source: function () {
        return manifest;
      },
      size: function () {
        return manifest.length;
      }
    };

    callback();
  });
};

module.exports = ScormManifestPlugin;
